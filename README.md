
This is Yubikey OTP 2FA for Zap.

Get your api username/key from https://upgrade.yubico.com/getapikey/

Put the username/key in Mod_YubiOTP.php

After you enable the app, go to /yubiotp and enter your key. This will initialize 
the 2FA and lock your account to the provided Yubikey. 

You can authorize a second device. Log in with the second device (ie, mobile phone).
You will be prompted to enter your Yubikey to proceed. 
On the primary device, which has already authenticated, return to the 
/yubiotp page and you will see the device pending at the top of the page, 
awaiting approval. Click APPROVE link and the second device will be 
authorized.

You can add multiple keys to your account.

Disable Yubikey 2FA by deleting all associated keys.

Questions?

Contact Waitman Gobble
ns@waitman.net
+1 829 914 3703

