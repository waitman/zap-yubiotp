<?php

namespace Zotlabs\Module;

/* get API key from 
	https://upgrade.yubico.com/getapikey/
*/

define('YUBIAPIUSER',123);
define('YUBIAPIKEY','abc');

use Zotlabs\Lib\Apps;
use Zotlabs\Lib\Libsync;

class YubiOTP extends \Zotlabs\Web\Controller {

	function get()
	{
		$channel = \App::get_channel();
		$loc = intval($channel['channel_account_id']);
		$show_yubiform = true;
		$another_ok = true;
		$o = '';

		if (intval($loc)<1)
		{
			notice( t('Please log in.') . EOL );
                        goaway(z_root() . '/');
		}

		$keyvalid = false;
		$error_message = '';
		$is_logged_in = $this->check_yubi_auth($loc);

		if ($is_logged_in && x($_REQUEST,'deletekey'))
		{
			$delkey = filter_var($_REQUEST['deletekey'],
						FILTER_SANITIZE_STRING);
			if ( strlen($delkey)==12 )
			{
				$serial_array = unserialize(get_pconfig($loc,'yubikey','serial'));
				$new_serial = array();
				foreach ($serial_array as $k=>$v)
				{
					if ($k!=$delkey)
					{
						$new_serial[$k]=true;
					}
				}
				if (count($new_serial)<1)
				{
					/* no longer using yubikey 2FA */
					$o = '<h1 style="color:Red;">Yubikey 2FA DISABLED</h1>';
					$show_yubiform = false;
					$another_ok = false;
					/* delete everything */
					setcookie('yubikeyauth','');
					set_pconfig($loc,'yubikey','yubikey_required',false);
					set_pconfig($loc,'yubikey','serial','');
					set_pconfig($loc,'yubikey','approved','');
					set_pconfig($loc,'yubikey','await','');
					set_pconfig($log,'yubikey','yubikeyauth','');
				} else {
					$o = '<h1 style="color:Green;">Key Deleted</h1>';
					set_pconfig($loc,'yubikey','serial',serialize($new_serial));
				}
				$show_yubiform = false;
			} else {
				$o = '<h1 style="color:Red;">ERROR</h1>
					<p>Invalid Yubikey OTP</p>';
			}
		}

		if (x($_POST,'yk'))
		{
			$yk = filter_var($_POST['yk'],FILTER_SANITIZE_STRING);
			if ( (strlen($yk)>12) && (strlen($yk)<64) )
			{
				/* check key */
				require_once 'Yubico.php';
				$yubi = new Auth_Yubico(YUBIAPIUSER,YUBIAPIKEY);
				$auth = $yubi->verify($yk);
				if ($auth) {
					if (!strstr($auth,'ERROR'))
					{
						/* this key is valid */
						/* check if new key */
						$serial_array = unserialize(get_pconfig($loc,'yubikey','serial'));
						if (is_array($serial_array) && (count($serial_array)>0))
						{
							if ($serial_array[substr($yk,0,12)])
							{
								$keyvalid = true;
								$show_yubiform = false;
								$authkey = base64_encode(random_bytes(64));
								set_pconfig($loc,'yubikey','authkey',$authkey);
								setcookie('yubikeyauth',$authkey);
								$o = '<h1>Authenticated</h1>
									<p>You have successfully authenticated</p>
									<p><a href="'.z_root().'/network">Continue to Network</a> &middot; 
										<a href="'.z_root().'/yubiotp">Manage Keys</a></p>';
							} else {
								$error_message =
                                                                        '<div style="color:Red;padding:10px;">
                                                                                <strong>WRONG KEY</strong></div>';
							}
						} else {
							/* NEW key */
							$serial = substr($yk,0,12);
							$keyvalid = true;
							$show_yubiform = false;
							$serial_array = array();
							$serial_array[$serial]=true;
							set_pconfig($loc,'yubikey','serial',serialize($serial_array));
							set_pconfig($loc,'yubikey','yubikey_required',true);
							$authkey = base64_encode(random_bytes(64));
							set_pconfig($loc,'yubikey','authkey',$authkey);
							setcookie('yubikeyauth',$authkey);

							$o = '<h1>Authenticated</h1>
								<p>You have successfully authenticated</p>
								<p><a href="'.z_root().'/network">Continue to Network</a> &middot;
									<a href="'.z_root().'/yubiotp">Manage Keys</a></p>';
						}
					} else {
						$error_message = 
							'<div style="color:Red;padding:10px;">
								<strong>'.$auth.'</strong></div>';
					}
				}
			} 
		} else {
			/* additional key */
			if ($is_logged_in && x($_POST,'ayk'))
	                {
				if (get_pconfig($loc,'yubikey','yubikey_required'))
				{
					$ayk = filter_var($_POST['ayk'],FILTER_SANITIZE_STRING);
					if ( (strlen($ayk)>12) && (strlen($ayk)<64) )
					{
						/* check key */
						require_once 'Yubico.php';
						$yubi = new Auth_Yubico(YUBIAPIUSER,YUBIAPIKEY);
						$auth = $yubi->verify($ayk);
						if ($auth) {
							if (!strstr($auth,'ERROR'))
							{
								$serial = substr($ayk,0,12);
								$serial_array = unserialize(get_pconfig($loc,'yubikey','serial'));
								$serial_array[$serial]=true;
								set_pconfig($loc,'yubikey','serial',serialize($serial_array));
								$o = '<h1 style="color:Green;">Key Added</h1>
									<p>You have added another Yubikey.</p>';
								$show_yubiform = false;
							} else {
								$show_yubiform = false;	
								$o = '<h1 style="color:Red;">ERROR</h1>
									<p>There was an error adding your key: '.$auth.'</p>';
							}
						} else {
							$show_yubiform = false;
							$o = '<h1 style="color:Red;">ERROR</h1>
								<p>Invalid Operation 1</p>';
						}
					} else {
						$show_yubiform = false;
						$o = '<h1 style="color:Red;">ERROR</h1>
							<p>Invalid Yubikey OTP</p>';
					}
				} else {
					$show_yubiform = false;
					$o = '<h1 style="color:Red;">ERROR</h1>
						<p>Invalid Operation 2</p>';
				}
			}

			/* if they are logged in then let them add another yubikey
				and delete them */
			if ($is_logged_in && $another_ok)
			{
				$show_yubiform = false;
				$keys = unserialize(get_pconfig($loc,'yubikey','serial'));
				$o .= '<h1>Manage Yubikey 2FA</h1>
';
				/* check for await */
				$s_info = unserialize(get_pconfig($loc,'yubikey','await'));
				$show_s_info = false;
				if (is_array($s_info))
				{
					$check_approved = get_pconfig($loc,'yubikey','approved');
					if ($check_approved!='')
					{
						if ($s_info['ma']==$check_approved)
						{
							// already approved
						} else {
							$show_s_info = true;
						}
					} else {
						$show_s_info = true;
					}
					if ($show_s_info)
					{
						/* check for manual approve */
						if (x($_REQUEST,'approve'))
						{
							set_pconfig($loc,'yubikey','approved',$_REQUEST['approve']);
							set_pconfig($loc,'yubikey','await','');
								$o .= '<p><strong>Client Approved</strong></p>';
						} else {
							$o .= '<div style="padding:10px;border:2px solid #ccc;">
<p><strong>Awaiting Confirmation</strong></p>
<p>IP: '.htmlentities($s_info['ip'].' UA: '.$s_info['ua']).'<br>
<a href="/yubiotp?approve='.urlencode($s_info['ma']).'">Approve (YES)</a></p>
</div>';
						}
					}
				}
		
$o .= '
<table border="1" cellspacing="0" cellpadding="3" width="500">
<tr><td><strong>Public ID</strong></td><td><strong>Remove</strong></td></tr>
';
				foreach ($keys as $key=>$alive)
				{
					$o .= '<tr>
<td>'.$key.'</td>
<td><a href="/yubiotp?deletekey='.urlencode($key).'">Delete(x)</a></td>
</tr>
';
				}
				$o .= '</table>
<p><br></p>
<p><strong>Add Another Yubikey</strong></p>
<form method="post" action="'.z_root().'/yubiotp">
Yubikey: <input type="text" name="ayk" value="" size="32" value=""> <button type="submit">Go</button>
</form>

';
			}
		}

		if ($show_yubiform)
		{
			$o = '<h1>Yubikey 2FA</h1>
'.$error_message.'
<form method="post" action="'.z_root().'/yubiotp">
Yubikey: <input type="text" name="yk" value="" size="32" value=""> <button type="submit">Go</button>
</form>
';
		}
		return $o;
	}


	/* consolidate auth check
		return true or false */
	function check_yubi_auth($loc)
	{

		if (x($_COOKIE,'yubikeyauth')&&(strlen($_COOKIE['yubikeyauth'])>64))
		{

			if ($_COOKIE['yubikeyauth'] == get_pconfig($loc,'yubikey','authkey'))
			{
				return true;
			}
		}

		/* check remote */
		if (x($_COOKIE,'marrow'))
		{
			$chk_approved = get_pconfig($loc,'yubikey','approved');
			if ($chk_approved!='')
			{
				if ($_COOKIE['marrow']==$chk_approved)
				{
					return true;
				}
			}
		}
		return false;
	}

}
