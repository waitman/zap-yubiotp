<?php

function check_yubi()
{
	if ($_SERVER['REQUEST_URI']=='/yubiotp')
	{
		return;
	}

	$channel = \App::get_channel();
	$loc = intval($channel['channel_account_id']);
	$is_approved = false;

	/* is the user logged in? */
	if ($loc>0)			
	{

                /* did the user choose to require yubikey 2FA? */
                if (get_pconfig($loc,'yubikey','yubikey_required'))
                {
			/* check remote approved */
			if (x($_COOKIE,'marrow'))
			{
				$chk_approved = get_pconfig($loc,'yubikey','approved');
				if ($chk_approved!='')
				{
					if ($_COOKIE['marrow']==$chk_approved)
					{
						$is_approved = true;
					}
				}
			}

			if (!$is_approved)
			{
				if (x($_COOKIE,'yubikeyauth')&&(strlen($_COOKIE['yubikeyauth'])>64))
				{
					$chk_auth = get_pconfig($loc,'yubikey','authkey');
					if ($chk_auth == $_COOKIE['yubikeyauth'])
					{
						$is_approved = true;
					}
				}
			}
	
			if (!$is_approved)
			{
				/* send the user to auth page */
				yubi_gateway($loc);
				Header("Location: /yubiotp");
				exit();
			}
		}
	} else {
		/*( they may have logged out )*/
		if (x($_COOKIE,'marrow'))
		{
			setcookie('marrow','');
		}
		if (x($_COOKIE,'yubikeyauth'))
		{
			setcookie('yubikeyauth','');
		}
	}
	return;
}

function yubi_gateway($loc)
{
	/* we can authorize another account from 
		an account that is already logged in
		example: approving a mobile device
		if you do not have the NFC yubikey */
					
                if (x($_COOKIE,'marrow'))
                {
                        // the user is already registered
                } else {
                        $reg = base64_encode(random_bytes(64));
                        setcookie('marrow',$reg);
                        $s_info = array();
                        $s_info['ip']=filter_var($_SERVER['REMOTE_ADDR'],
                                                FILTER_SANITIZE_STRING);
                        $s_info['ua']=filter_var($_SERVER['HTTP_USER_AGENT'],
                                                FILTER_SANITIZE_STRING);
                        $s_info['ma']=$reg;
                        set_pconfig($loc,'yubikey','await',serialize($s_info));
                }
	return;
}
