<?php

/**
 * Name: YubiOTP App
 * Description: Provide 2FA using Yubico OTP
 * Version: 1.0
 * MinVersion: 1.4.2
 * Author: Waitman Gobble <waitman@waitman.net>
 * Maintainer: Me
 */

use \Zotlabs\Extend\Route;
use \Zotlabs\Extend\Hook;

function yubiotp_load() {
        Route::register('addon/yubiotp/Mod_YubiOTP.php','yubiotp');
	Hook::register('build_pagehead', 'addon/yubiotp/authcheck.php', 'check_yubi');
}

function yubiotp_unload() {
        Route::unregister_by_file('addon/yubiotp/Mod_YubiOTP.php');
	Hook::unregister_by_file('addon/yubiotp/authcheck.php');
}

function yubiotp_app_menu(&$b) {
	$b['app_menu'][] = '<div class="app-title"><a href="/yubiotp">Yubi 2FA</a></div>'; 
}


